# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Generate the cassini wic image
IMAGE_FSTYPES += "wic wic.gz wic.bmap"

# Remove initramfs image generation for cassini distribution due to size restrictions
INITRAMFS_IMAGE:remove:cassini =  "corstone1000-initramfs-image"

# Extend the wks search path to find custom wks file
WKS_SEARCH_PATH:prepend:cassini := "${CASSINI_ARM_BSP_DYNAMIC_DIR}/wic:"
WKS_FILE:cassini = "corstone1000-efidisk.wks.in"
GRUB_CFG_FILE:cassini ?= "${CASSINI_ARM_BSP_DYNAMIC_DIR}/wic/corstone1000-grub.cfg"

# Add grub-efi
EFI_PROVIDER:cassini ?= "grub-efi"
MACHINE_FEATURES:append:cassini = " efi"

# Due to performance limitations, K3S is not supported on this platform
DISTRO_FEATURES:remove = "k3s"
IMAGE_INSTALL:remove = "k3s-server k3s-integration-tests-ptest"
KERNEL_CLASSES:remove = "k3s_kernelcfg_check"

# Due to performance limitations, add wait-online helper
IMAGE_INSTALL:append:cassini-test = "wait-online"
