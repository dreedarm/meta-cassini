# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

include conf/machine/include/corstone1000-mps3-cassini-extra-settings.inc

# FVP parameters
# Corstone-1000's FVP need images to have a extra empty space at the end or the FVP
# truncates the mmc card size resulting in invalid GPT entries as the GPT last block is
# beyond the MMC cards reported size, so we use the padded image here
FVP_CONFIG[board.msd_mmc.p_mmc_file] = "${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}.wic.pad"
# Setup the second MMC card
FVP_CONFIG[board.msd_mmc_2.card_type] ?= "SDHC"
FVP_CONFIG[board.msd_mmc_2.p_fast_access] ?= "0"
FVP_CONFIG[board.msd_mmc_2.diagnostics] ?= "0"
FVP_CONFIG[board.msd_mmc_2.p_max_block_count] ?= "0xFFFF"
FVP_CONFIG[board.msd_config_2.pl180_fifo_depth] ?= "16"

FVP_DATA = "board.flash0=${DEPLOY_DIR_IMAGE}/corstone1000-image-corstone1000-fvp.wic@0x68000000"
