# nooelint: oelint.var.mandatoryvar - This recipe has no source files
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Trusted Services block storage service provider"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

require ts-sp-common.inc
require recipes-security/trusted-services/ts-arm-platforms.inc

SP_UUID = "${BLOCK_STORAGE_UUID}"
TS_SP_BLOCK_STORAGE_CONFIG ?= "default"

OECMAKE_SOURCEPATH = "${S}/deployments/block-storage/config/${TS_SP_BLOCK_STORAGE_CONFIG}-${TS_ENV}"
