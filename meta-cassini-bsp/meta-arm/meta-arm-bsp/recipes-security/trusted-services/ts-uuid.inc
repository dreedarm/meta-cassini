# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Trusted Services SPs canonical UUIDs

BLOCK_STORAGE_UUID = "63646e80-eb52-462f-ac4f-8cdf3987519c"
