# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Include Trusted Services Secure Partitions according to defined machine features

# From meta-cassini
require ../trusted-services/ts-uuid.inc

# Block Storage SP
DEPENDS:append:n1sdp  = "${@bb.utils.contains('MACHINE_FEATURES', 'ts-block-storage', \
                                        ' ts-sp-block-storage', '' , d)}"

# Load Block Storage SP first
SP_PATHS:prepend:n1sdp = "${@bb.utils.contains('MACHINE_FEATURES', 'ts-block-storage', \
                                        ' ${TS_BIN}/${BLOCK_STORAGE_UUID}.stripped.elf', '', d)}"
