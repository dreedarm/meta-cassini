# SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Fetch ptest-runner 2.4.2 release tag
# nooelint: oelint.append.protvars.SRCREV - Defect fix not in base revision
SRCREV:cassini = "bcb82804daa8f725b6add259dcef2067e61a75aa"
