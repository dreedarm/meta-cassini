# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "psa-arch-tests (aka psa api tests)"
DESCRIPTION = "psa-arch-tests is a test suite to tests different services  \
               provided by Trusted Services. These tests are run from linux \
               userspace. Please refer to https://github.com/ARM-software/psa-arch-tests \
               for more information. "
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = "\
    file://psa-arch-tests.bats \
    "

inherit runtime-integration-tests
require runtime-integration-tests.inc
