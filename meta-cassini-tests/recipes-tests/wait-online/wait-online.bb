# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Wait-Online Helper"
DESCRIPTION = "Wait-Online Helper to help ensure network services are ready \
    on systems with limited performance (FPGA/FVP)."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"

# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://wait-online.sh"

S = "${WORKDIR}/home"

inherit allarch features_check

FILES:${PN} += "${bindir}/wait-online.sh"

RDEPENDS:${PN}:append = " bash wget"

REQUIRED_DISTRO_FEATURES = "systemd"

do_install() {
    install -d "${D}/${bindir}"
    install --mode="755" "${WORKDIR}/wait-online.sh" "${D}/${bindir}/wait-online.sh"
}
