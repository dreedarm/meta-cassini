..
 # Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
 #
 # SPDX-License-Identifier: MIT

##################
Security Hardening
##################

Cassini distribution images can be hardened to reduce potential sources or
attack vectors of security vulnerabilities. Cassini security hardening modifies
the distribution to:

  * Force password update for each user account after first logging in.
    An empty and expired password is set for each user account by default.
  * Enhance the kernel security, kernel configuration is extended with the
    ``security.scc`` in ``KERNEL_FEATURES``.
  * Enable the 'Secure Computing Mode' (seccomp) Linux kernel feature by
    appending ``seccomp`` to ``DISTRO_FEATURES``.
  * Ensure that all available packages from ``meta-openembedded`` and
    ``poky`` layers are configured with:
    ``--with-libcap[-ng]``.
  * Remove ``debug-tweaks`` from ``IMAGE_FEATURES``.
  * Disable all login access to the ``root`` account.

    .. note::
      When ``cassini-test`` distro feature is enabled then ``root`` login is
      enabled. Currently, running ``inline tests`` in LAVA require login as
      ``root`` to run `transfer-overlay <https://docs.lavasoftware.org/lava/actions-boot.html#transfer-overlay>`_
      commands.
  * Sets the umask to ``0027`` (which translates permissions as ``640`` for
    files and ``750`` for directories).

Security hardening is not enabled by default, see
:ref:`security_hardening_label` for details on including the
security hardening on the Cassini distribution image.

.. note::
  Cassini security hardening does not reduce the scope of the
  :ref:`run-time_integration_tests_label`.
