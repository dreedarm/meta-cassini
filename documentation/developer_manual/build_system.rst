..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Build System
############

A Cassini distribution can be built by setting the target platform via the
``MACHINE`` BitBake variable. In addition, the desired distribution features
via the ``DISTRO_FEATURES`` BitBake variable. Finally, customizing those
features via feature-specific modifiable variables, if needed.

This chapter provides an overview of Cassini's support for the kas build tool.
All the available distribution image features and supported target platforms
are defined together with their associated kas configuration files, followed by
any other additional customization options. The process for building without
kas is then briefly described.

**********************
kas Build Tool Support
**********************

The kas build tool enables automatic fetch and inclusion of layer sources, as
well as parameter and feature specifications for building target images. To
enable this, kas configuration files in the YAML format are passed to the tool
to provide the necessary definitions.

These kas configuration files are modular, where passing multiple files will
result in an image produced with their combined configuration. Further, kas
configuration files can extend other kas configuration files, thereby enabling
specialized configurations that inherit common configurations.

The ``meta-cassini-config/kas`` directory contains kas configuration files that
support building images via kas for the Cassini project, and fall into three
ordered categories:

* **Base Config**
* **Build Modifier Configs**
* **Target Platform Configs**

To build an Cassini distribution image via kas, it is required to provide the
**Base Config** and one **Target Platform Config**, unless otherwise stated
in their descriptions below. Additional **Build Modifier Configs** are
optional, and depend on the target use-case. Currently, it is necessary that
kas configuration files are provided in order: The **Base Config** and then
additional build features via zero or more **Build Modifier Configs**, and
finally the **Target Platform Config**.

To enable builds for a supported target platform or configure each Cassini
distribution image feature, kas configurations files are described in their
relevant sections below: :ref:`target_platforms_label` and
:ref:`distribution_image_features_label`, respectively. Example usage of these
kas configuration files can be found in the :ref:`build_label`
section of the User Manual.

.. note::
  If a kas configuration file does not set a particular build parameter, the
  parameter will take its default value.

.. _target_platforms_label:

****************
Target Platforms
****************

Neoverse N1 System Development Platform (N1SDP)
===============================================

  * **Corresponding value for** ``MACHINE`` **variable**: ``n1sdp``.
  * **Target Platform Config**: ``meta-cassini-config/kas/n1sdp.yml``.

  This supported target platform is the Neoverse N1 System Development Platform
  (N1SDP), implemented in |meta-arm-bsp|_.

  To read documentation about the N1SDP, see the
  |N1SDP Technical Reference Manual|_.

.. note::
  Support for the N1SDP platform in Cassini is primarily intended for
  development, test, and demonstration of features for infrastructure
  platforms which typically use EDK2 and Trusted Services without a
  secure enclave.

  Due to a limitation of the platform hardware (it does not have
  enough Secure world RAM) Trusted Services is configured to run
  from Normal world RAM. This configuration is not compliant with
  the PSA specifications.

  Platforms intended for production should be configured by the
  platform provider to be compliant with the PSA specifications.

Corstone-1000 for MPS3
======================

  * **Corresponding value for** ``MACHINE`` **variable**: ``corstone1000-mps3``
  * **Target Platform Config**: ``meta-cassini-config/kas/corstone1000-mps3.yml``

  This supported target platform is the Corstone-1000 for MPS3, implemented in
  |meta-arm-bsp|_.

  To read documentation about the Corstone-1000, see the
  |Arm Corstone-1000 Technical Overview|_.

  For more information about the software stack for the Corstone-1000, see
  |Arm Corstone-1000 Software|

Corstone-1000 FVP
=================

  * **Corresponding value for** ``MACHINE`` **variable**: ``corstone1000-fvp``
  * **Target Platform Config**: ``meta-cassini-config/kas/corstone1000-fvp.yml``

    This supported target platforms is the Corstone-1000 FVP, implemented in
    |meta-arm-bsp|_.

  To read documentation about the Corstone-1000 FVP, see the
  |Fast Models Fixed Virtual Platforms (FVP) Reference Guide|_.

``n1sdp.yml``, ``corstone1000-fvp.yml``, and ``corstone1000-mps3.yml``
**Target Platform Config** includes common configuration from
``meta-cassini-config/kas/include/arm-machines.yml`` which defines the
BSPs, layers, and dependencies required when building for each platform.

.. note::
  Due to performance limitations, K3S is not currently supported on
  the Arm Corstone-1000.

.. _distribution_image_features_label:

***************************
Distribution Image Features
***************************

For a particular target platform, the available Cassini distribution image
features (corresponding to the contents of the ``DISTRO_FEATURES`` BitBake
variable) are detailed in this section, along with any associated kas
configuration files, and any associated customization options relevant for that
feature.

Cassini Architecture
====================

Cassini distribution image can be configured via kas using **Base Config**.
This includes a set of common configuration from a base Cassini kas
configuration file:

  * ``meta-cassini-config/kas/include/cassini-base.yml``

  This kas configuration file defines the base Cassini layer dependencies and
  their software sources, as well as additional build configuration variables.
  It also includes the ``meta-cassini-config/kas/include/cassini-release.yml``
  kas configuration file, where the layers dependencies are pinned for any
  corresponding Cassini release.

  * **Corresponding value in** ``DISTRO`` **variable**:
    ``cassini``.
  * **Base Config**: ``meta-cassini-config/kas/cassini.yml``.

  This Cassini distribution image feature enables the ``cassini-image-base``
  build target, to build an Cassini distribution image.

  The **Base Config** for this distribution image feature sets the build
  target to ``cassini-image-base``.

  To build Cassini distribution image, provide the **Base Config** to the kas
  build command. For example, to build a Cassini distribution image for the
  N1SDP hardware target platform, run the following command:

    .. code-block:: console

      kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/n1sdp.yml

Other Cassini Features
======================

Developer Support
-----------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-dev``.
  * **Base Config**: ``meta-cassini-config/kas/cassini-dev.yml``.

  This Cassini distribution feature includes packages appropriate for
  development image builds, such as the ``debug-tweaks`` package, which sets an
  empty root password for simplified development access.

    .. code-block:: console

      kas build meta-cassini-config/kas/cassini-dev.yml:meta-cassini-config/kas/n1sdp.yml

.. _build_system_run-time_integration_tests_label:

Run-Time Integration Tests
--------------------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-test``.
  * **Build Modifier Config**: ``meta-cassini-config/kas/tests.yml``.

    This Cassini distribution feature includes the Cassini test suites
    provided to validate the image is running successfully with the expected
    Cassini functionalities.

    The Build Modifier for this distribution image feature automatically
    includes the Yocto Package Test (ptest) framework in the image, configures
    the inclusion of ``meta-cassini-tests`` as a Yocto layer source for the
    build, and appends the ``cassini-test`` feature to ``DISTRO_FEATURES`` for
    the build.

    To include run-time integration tests in a Cassini distribution image,
    provide the **Build Modifier Config** to the kas build command. For
    example, to include the tests in a Cassini distribution image for the N1SDP
    hardware target platform, run the following command:

    .. code-block:: console

      kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/tests.yml:meta-cassini-config/kas/n1sdp.yml

    Each suite of run-time integration tests and specific customizable
    variables associated with each suite are detailed separately, at
    :ref:`run-time_integration_tests_label`.

.. _parsec_service_label:

Parsec service
--------------

    **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-parsec``.

    This Cassini distribution feature adds parsec-service and parsec-tool
    to the Cassini distribution image.

    The value ``cassini-parsec`` is appended to ``DISTRO_FEATURES`` in
    ``meta-cassini-distro/conf/distro/cassini.conf``. Therefore, parsec service
    is included in the Cassini distribution image by default. If parsec-service
    is not required then the value ``cassini-parsec`` can be removed from
    ``DISTRO_FEATURES`` in the ``<distro name>.conf`` of the downstream
    distribution. To build Cassini distribution image with parsec-service for
    the N1SDP hardware target platform, run the following command:

    .. code-block:: console

      kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/n1sdp.yml

.. _security_hardening_label:

Security Hardening
------------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-security``.
  * **Build Modifier Config**: ``meta-cassini-config/kas/security.yml``.

    This Cassini distribution feature configures user accounts, packages,
    remote access controls and other image features to provide extra security
    hardening for the Cassini distribution image.

    To include extra security hardening in a Cassini distribution image,
    provide the **Build Modifier Config** to the kas build command, which
    appends the ``cassini-security`` feature to ``DISTRO_FEATURES`` for the
    build. For example, to include it in the Cassini distribution image for the
    N1SDP hardware target platform, run the following command:

    .. code-block:: console

      kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/security.yml:meta-cassini-config/kas/n1sdp.yml

    The security hardening is described in more detail at
    :ref:`Security Hardening<developer_manual/security_hardening:Security Hardening>`.

.. _software_development_kit_label:

Software Development Kit (SDK)
------------------------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-sdk``.
  * **Build Modifier Config**: ``meta-cassini-config/kas/cassini-sdk.yml``

    This Cassini distribution feature:

      * Adds the Cassini Software Development Kit (SDK) which includes packages
        and image features to support on-target software development activities.
      * Enables an additional SDK build target, ``cassini-image-sdk``

    The Build Modifier for this distribution image feature automatically
    appends ``cassini-sdk`` to ``DISTRO_FEATURES``, and sets the appropriate
    build target with the necessary configuration included by default.

    To include the SDK in a Cassini distribution image, provide the appropriate
    SDK **Build Modifier Config** to the kas build command. For example, to
    include the SDK in a Cassini distribution image for the N1SDP hardware
    target platform, run the following command:

    .. code-block:: console

      kas build meta-cassini-config/kas/baremetal-sdk.yml:meta-cassini-config/kas/n1sdp.yml

    The SDK itself is described in more detail at
    :ref:`Software Development Kit (SDK)<developer_manual/sdk:Software Development Kit (SDK)>`.

********************************************
Additional Distribution Image Customizations
********************************************

An additional set of customization options are available for Cassini
distribution images, which don't fall under a distinct distribution image
feature. These customizations are listed below and are grouped by the
customization target.

Filesystem Customization
========================

Adding Extra Rootfs Space
-------------------------

The size of the root filesystem can be extended via the
``CASSINI_ROOTFS_EXTRA_SPACE`` BitBake variable, which defaults to ``2000000``
Kilobytes. The value of this variable is appended to the
``IMAGE_ROOTFS_EXTRA_SPACE`` BitBake variable.

Tuning the Filesystem Compilation
---------------------------------

The Cassini filesystem by default uses the generic ``armv8a-crc`` tune for
``aarch64`` based target platforms. This reduces build times by increasing the
sstate-cache reused between different image types and target platforms. This
optimization can be disabled by setting ``CASSINI_GENERIC_ARM64_FILESYSTEM`` to
``"0"``. The file system compilation tune used when
``CASSINI_GENERIC_ARM64_FILESYSTEM`` is enabled can be changed by setting
``CASSINI_GENERIC_ARM64_DEFAULTTUNE``, which configures the ``DEFAULTTUNE``
BitBake variable for the ``aarch64`` based target platforms builds. See
|DEFAULTTUNE|_ for more information.

In summary, the relevant variables and their default values are:

  .. code-block:: yaml

    CASSINI_GENERIC_ARM64_FILESYSTEM: "1"             # Enable generic file system (1 or 0).
    CASSINI_GENERIC_ARM64_DEFAULTTUNE: "armv8a-crc"   # Value of DEFAULTTUNE if generic file system enabled.

Their values can be set by passing them as environmental variables. For
example, the optimization can be disabled using:

  .. code-block:: console

        CASSINI_GENERIC_ARM64_FILESYSTEM="0" kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/n1sdp.yml

**************************
Manual BitBake Build Setup
**************************

In order to build an Cassini distribution image without the kas build tool
directly via BitBake, it is necessary to prepare a BitBake project as follows:

  * Configure :ref:`dependent Yocto layers <layer_dependency_overview_label>`
    in ``bblayers.conf``.
  * Configure the ``DISTRO`` as ``cassini`` in ``local.conf``.
  * Configure the image ``DISTRO_FEATURES`` in ``local.conf``.

Assuming correct environment configuration, the BitBake build can then be run
for the desired image target corresponding to one of the following:

  * ``cassini-image-base``
  * ``cassini-image-sdk``

As the kas build configuration files within the ``meta-cassini-config/kas/``
directory define the recommended build settings for each feature. Any
additional functionalities may therefore be enabled by reading these
configuration files and manually inserting their changes into the BitBake build
environment.
