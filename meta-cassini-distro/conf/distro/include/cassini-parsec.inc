# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-parsec distro feature, enabled using
# DISTRO_FEATURES

# Check for platforms with or without secure enclave
PACKAGECONFIG:pn-parsec-service = "${@bb.utils.contains('MACHINE_FEATURES', 'ts-crypto ts-its', \
                                    'TS', bb.utils.contains('MACHINE_FEATURES', 'ts-se-proxy', 'TS', '', d), d)}"

IMAGE_INSTALL:append = " parsec-service \
                         parsec-tool"

DISTROOVERRIDES .= ":cassini-parsec"
