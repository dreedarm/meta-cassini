# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-sdk distro feature, enabled using
# DISTRO_FEATURES

include cassini-dev.inc

IMAGE_FEATURES:append:cassini = " \
 package-management \
 dev-pkgs \
 tools-sdk \
 tools-debug \
 tools-profile \
 ssh-server-openssh"

IMAGE_INSTALL:append:cassini = " kernel-base kernel-devsrc kernel-modules"

IMAGE_INSTALL:append:aarch64:cassini = " gator-daemon"

DISTROOVERRIDES .= ":cassini-sdk"
