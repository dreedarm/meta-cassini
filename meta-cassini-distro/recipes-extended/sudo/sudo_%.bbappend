# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:cassini := "${THISDIR}/files:"

DEPENDS:append:cassini = " gettext-native"

SRC_URI:append:cassini = " file://cassini_admin_group.in"

do_install:append:cassini() {

    export ADMIN_GROUP="${CASSINI_ADMIN_GROUP}"
    export ADMIN_GROUP_OPTIONS="${CASSINI_ADMIN_GROUP_OPTIONS}"

    envsubst < ${WORKDIR}/cassini_admin_group.in > ${D}${sysconfdir}/sudoers.d/cassini_admin_group
    chmod 644 ${D}${sysconfdir}/sudoers.d/cassini_admin_group
}
