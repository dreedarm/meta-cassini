# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}debian:buster-slim

ARG local_user=cassini-ci
ARG user_id=999
ARG group_id=999

RUN apt-get update && \
    apt-get install -y locales && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.utf8

RUN apt-get install --no-install-recommends -y \
    ca-certificates \
    curl \
    git \
    jq \
    lavacli \
    libssl-dev \
    libz-dev \
    openssh-client \
    openssl \
    python3 \
    python3-setuptools \
    python3-pip \
    zip \
  && apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN python3 -m pip install \
    j2cli \
    j2cli[yaml] \
    junit-xml \
    markdown

RUN groupadd -g $group_id $local_user && useradd --no-log-init -m -d /builder -g $local_user -u $user_id $local_user \
  && chown -R $local_user:$local_user /builder \
  && cd /usr/local/bin \
  && curl -fL https://getcli.jfrog.io/v2 | sh \
  && chmod a+x /usr/local/bin/*

USER $local_user

RUN cd /builder \
  && git clone https://github.com/dreedarm/SCT_Parser.git \
  && cd SCT_Parser \
  && git checkout junit
