# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}ubuntu:jammy

ARG MODEL="Corstone-1000-23"
ARG MODEL_CODE="FVP_Corstone_1000"
ARG MODEL_VERSION="11.19_21"
ARG FVP_ARCH="Linux64"
ARG SRC_URI="https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-1000-23/Linux/FVP_Corstone_1000_11.19_21_Linux64.tgz"

RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    bc \
    bridge-utils \
    ca-certificates \
    curl \
    dnsmasq \
    gnupg2 \
    iproute2 \
    libatomic1 \
    libdbus-1-3 \
    libvirt-clients \
    libvirt-daemon-system \
    telnet \
    tree \
    software-properties-common \
    unzip \
    wget \
    && apt-get clean \
    && rm -rf /var/cache/apt

# Create model directory
RUN mkdir /opt/model

# Setup networking
COPY network /etc/libvirt/hooks/
COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh \
    && chmod 755 /etc/libvirt/hooks/network
ENTRYPOINT ["/entrypoint.sh"]

# Add FVP and Testing Binaries
ADD "${SRC_URI}" /tmp/

RUN cd /tmp \
    && tar -zxvf "/tmp/${MODEL_CODE}_${MODEL_VERSION}_${FVP_ARCH}.tgz" \
    && ./FVP_Corstone_1000.sh --i-agree-to-the-contained-eula \
        --no-interactive \
        --destination /opt \
    && rm -rf /tmp/*

EXPOSE 5000/tcp
EXPOSE 5001/tcp
EXPOSE 5002/tcp
EXPOSE 5003/tcp
