# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
.test-preparation:
  extends: .resource-request
  stage: Test
  image: $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG
  variables:
    DOCKER_IMAGE_NAME: lava-test-image
    KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 5Gi
  rules:
    - if: $LAVA_URL
  script:
    - mkdir cassini-firmware
    - pushd cassini-firmware
    - job_url=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}
    - data_file=${CI_PROJECT_DIR}/build_data.env
    - |
      case "${MACHINE}" in
        'n1sdp')
          echo "Building SD Card image for N1SDP"
          fw_file=firmware.zip
          # Update the PMIC_FORCE parameter in the config.txt file in the
          # firmware package to TRUE.
          # Setting it to FALSE will require user input so the boot process
          # halts.
          fw_tar=${FW_IMAGE}-board-firmware_primary.tar.gz
          tar -zxvf ${CI_PROJECT_DIR}/${ARTIFACT_DIR}/${fw_tar}
          config="PMIC_FORCE:"
          current_value=" FALSE"
          new_value=" TRUE"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          config="MCPUART:"
          current_value="   1"
          new_value="   0"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          config="FPGAUART2:"
          current_value=" 0"
          new_value=" 1"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          zip -r ${CI_PROJECT_DIR}/${fw_file} *
          echo "FIRMWARE_ARTIFACT_URL=${job_url}/artifacts/${fw_file}" \
            >> ${data_file}
          ;;
        'corstone1000-mps3')
          echo "Building images for Corstone-1000 for MPS3"
          fw_file=firmware.zip
          git clone --branch v2 \
            https://${CI_SERVER_HOST}/${CORSTONE1000_BITFILE_PATH}
          export \
          BUILD_DIR=${CI_PROJECT_DIR}/${ARTIFACT_DIR}
          export WIC_NAME="${FW_IMAGE}-${MACHINE}.wic"
          # Create restore image directory structure
          mkdir --parents cs1k-restore/SOFTWARE cs1k-restore/MB
          # Copy required files to correct place
          cp ${BUILD_DIR}/${WIC_NAME} \
            cs1k-restore/SOFTWARE/cs1000.bin
          cp ${BUILD_DIR}/${BL1_IMAGE} \
            cs1k-restore/SOFTWARE/bl1.bin
          cp ${BUILD_DIR}/${EXTSYS_IMAGE} \
            cs1k-restore/SOFTWARE/es0.bin
          cp -r corstone1000-bitfiles/Boardfiles/MB/* \
            cs1k-restore/MB/
          cp corstone1000-bitfiles/Boardfiles/config.txt \
            cs1k-restore/config.txt
          # Generate new configs
          find cs1k-restore/MB -type f -name 'images.txt' | xargs \
            sed -i -e "s/^TOTALIMAGES:.*/TOTALIMAGES: 3/g" \
              -e "s/^IMAGE0FILE:.*/IMAGE0FILE: \\\\SOFTWARE\\\\bl1.bin/g" \
              -e "s/^IMAGE1FILE:.*/IMAGE1FILE: \\\\SOFTWARE\\\\es0.bin/g" \
              -e "s/^IMAGE2FILE:.*/IMAGE2FILE: \\\\SOFTWARE\\\\cs1000.bin/g"
          find cs1k-restore/MB -type f -name 'images.txt' | xargs cat
          sed -i -e "s/^AUTORUN: FALSE/AUTORUN: TRUE /g" cs1k-restore/config.txt
          cat cs1k-restore/config.txt
          # Now zip the new fpga bitfiles
          pushd cs1k-restore
          zip -r ${CI_PROJECT_DIR}/${fw_file} *
          popd  # cs1k-restore
          echo "FIRMWARE_ARTIFACT_URL=${job_url}/artifacts/${fw_file}" \
            >> ${data_file}
          ;;
        'corstone1000-fvp')
          echo "Nothing to be done for Corstone-1000 FVP"
          echo "FIRMWARE_ARTIFACT_URL=${FW_IMAGE_ARTIFACT_URL}" >> ${data_file}
          ;;
          *)
          echo "Unknown machine ${MACHINE}"
          exit 1
          ;;
      esac
    - popd  # cassini-firmware
    - cat ${CI_PROJECT_DIR}/build_data.env
  artifacts:
    paths:
      - "build_data.env"
      - "firmware.zip"
      - "**/*.fvpconf"
    reports:
      dotenv: build_data.env
    expire_in: "1 days"

.submit-cassini-lava-job:
  extends: .submit-lava-job
  variables:
    FIRMWARE_ARTIFACT: ${FIRMWARE_ARTIFACT_URL}
    BMAP_ARTIFACT: ${BMAP_ARTIFACT_URL}
    IMAGE_ARTIFACT: ${IMAGE_ARTIFACT_URL}
    UTIL_ARTIFACT: ${UTIL_IMAGE_ARTIFACT_URL}
    BL1_ARTIFACT: ${BL1_IMAGE_ARTIFACT_URL}
    EXTSYS_ARTIFACT: ${EXTSYS_IMAGE_ARTIFACT_URL}
    LAVA_JOB_TEMPLATE: .gitlab/lava/$MACHINE/$LAVA_JOB.j2
  script:
    # Fill in job template with image locations
    - |
      if [ ${MACHINE} == 'corstone1000-fvp' ]; then
        j2 ${LAVA_JOB_TEMPLATE} -o ${LAVA_JOB}.tmp
        pushd ${ARTIFACT_DIR}
        FVP_CONFIG_FILE=$(readlink ${FVP_CONFIG}-${MACHINE}.fvpconf)
        popd
        python3 .gitlab/scripts/fvpconf-to-lava.py \
          "${ARTIFACT_DIR}/${FVP_CONFIG_FILE}" \
          "${LAVA_JOB}.tmp" \
          "${LAVA_JOB}.tmp.j2"
        export LAVA_JOB_TEMPLATE="${LAVA_JOB}.tmp.j2"
      fi
    - !reference [".submit-lava-job", script]
  artifacts:
    paths:
      - test_data.env
      - $LAVA_JOB
      - $LAVA_JOB.tmp
      - $LAVA_JOB.tmp.j2
    reports:
      dotenv: test_data.env
    expire_in: 1 day

.ptest-tests:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: ptest.yml

.ptest-tests-results:
  extends: .complete-lava-job
  after_script:
    - |
      jfrog config add artifactory-aws --interactive=false \
      --artifactory-url=$ARTIFACTORY_AWS_URL --user=$ARTIFACTORY_USER \
      --password=$ARTIFACTORY_PASS;
    - echo LAUNCH_CI_JOB_NAME_AND_ID="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - job_info="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - build_id="oss-cassini/${CI_PROJECT_PATH}/${job_info}"
    - |
      cat << EOF > ./download_spec.json
      {
        "files": [
          {
            "pattern": "oss-cassini.lava-images-temp/ptest-runner-results/",
            "build": "${build_id}",
            "flat": "false",
            "recursive": "true"
          }
        ]
      }
      EOF
    - cat download_spec.json
    - jfrog rt download --spec=download_spec.json --fail-no-op
    - |
      python3 .gitlab/scripts/ptest-to-junit.py \
      ptest-runner-results \
      TEST-ptest-runner-test-report.xml
  artifacts:
    paths:
      - ptest-runner-results/**/*
      - TEST-*-test-report.xml
      - lava-result.yml
    expire_in: 1 day
    reports:
      junit:
        - TEST-*-test-report.xml
    when: always


.acs-test:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: acs_suite.yml
    IR_IMG: "ir-acs-live-image-generic-arm64.wic"
    GITHUB_REPO: "https://github.com/ARM-software/arm-systemready"
    IMAGE_ARTIFACT:
      "$GITHUB_REPO/raw/main/IR/prebuilt_images/v23.03_2.0.0/$IR_IMG.xz"
  rules:
    - if: $MACHINE == "n1sdp"
      variables:
        IR_IMG: "ir_acs_live_image-${MACHINE}.img"
        BMAP_ARTIFACT:
          "$ARTIFACTORY_URL/oss-cassini.lava-images-temp/ACS/$IR_IMG.bmap"
        IMAGE_ARTIFACT:
          "$ARTIFACTORY_URL/oss-cassini.lava-images-temp/ACS/$IR_IMG.bz2"
    - when: always

.acs-test-results:
  extends: .complete-lava-job
  after_script:
    - |
      jfrog config add artifactory-aws --interactive=false \
      --artifactory-url=$ARTIFACTORY_AWS_URL --user=$ARTIFACTORY_USER \
      --password=$ARTIFACTORY_PASS;
    - echo LAUNCH_CI_JOB_NAME_AND_ID="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - job_info="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - build_id="oss-cassini/${CI_PROJECT_PATH}/${job_info}"
    - echo SEQUENCE_FILE=$SEQUENCE_FILE
    - |
      cat << EOF > ./download_spec.json
      {
        "files": [
          {
            "pattern": "oss-cassini.lava-images-temp/acs_results/",
            "build": "${build_id}",
            "flat": "false",
            "recursive": "true"
          }
        ]
      }
      EOF
    - cat download_spec.json
    - jfrog rt download --spec=download_spec.json --fail-no-op
    - |
      if [ -e ./acs_results/uefi/BsaResults.log ]; then
        python3  .gitlab/scripts/BSA-to-junit.py \
        ./acs_results/uefi/BsaResults.log \
        TEST-BSA-test-report.xml
      fi
    - |
      if [ -e ./acs_results/fwts/FWTSResults.log ]; then
        python3  .gitlab/scripts/fwts-to-junit.py \
        ./acs_results/fwts/FWTSResults.log \
        TEST-FWTS-test-report.xml
      fi
    - python3 /builder/SCT_Parser/parser.py
      ./acs_results/sct_results/Overall/Summary.ekl
      ./acs_results/sct_results/Sequence/EBBR.seq
      --junit TEST-SCT-${SEQUENCE_FILE}-test-report.xml
  artifacts:
    paths:
      - lava-console.log
      - acs_results/**
      - TEST-*-test-report.xml
      - lava-result.yml
    expire_in: 1 week

.sanity-test:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: sanity_job.yml

.sanity-test-results:
  extends: .complete-lava-job
  artifacts:
    paths:
      - lava-result.yml
    expire_in: 1 day
    reports:
      junit:
        - TEST-*-test-report.xml
    when: always
